package com.skobkin.copypasteclient;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class CopyPaste extends Activity implements View.OnClickListener {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        View button_send = findViewById(R.id.button_send);
        button_send.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        //To change body of implemented methods use File | Settings | File Templates.
        EditText txt_url = (EditText) findViewById(R.id.txt_url);
        txt_url.setText(SendPaste());
    }

    public String SendPaste()
    {
        EditText txt_code = (EditText) findViewById(R.id.txt_code);

        String api = "http://code.skobkin.ru/api.php";
        String result = new String("");

        try{
            URLConnection connection = null;
            URL url = new URL(api);

            connection = url.openConnection();

            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("User-Agent", "CP Client 0.1");
            httpConnection.setRequestProperty("Content-Language", "ru-RU");
            httpConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            httpConnection.setDoOutput(true);
            httpConnection.setDoInput(true);

            httpConnection.connect();

            OutputStream os = httpConnection.getOutputStream();

            String req = "mode=text&code=" + txt_code.getText();
            os.write(req.getBytes());

            os.flush();
            os.close();

            int response = httpConnection.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK)
            {
                InputStream in = httpConnection.getInputStream();


                InputStreamReader isr = new InputStreamReader(in, "UTF-8");

                StringBuffer data = new StringBuffer();
                int c;
                while ((c = isr.read()) != -1)
                {
                    data.append((char) c);
                }

                result = new String(data.toString());

            }
            else
            {
                result = "Error: no response";
            }
        }
        catch (MalformedURLException e)
        {

        }

        catch (IOException e) {
            result = "IOException:" + e.getMessage();
        }

        return result;
    };
}
